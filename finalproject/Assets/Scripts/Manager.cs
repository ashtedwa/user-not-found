﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Manager : MonoBehaviour {

    public Button button;
    public Text NameText;
    public static bool TumblrDone = false;
    public static bool YoutubeDone = false;
    public static bool FacebookDone = false;

    static string Name = "";



    public void ChangeName(string CurrentName )
    {
        Name = CurrentName;
        Debug.Log(Name);
    }

    

    // Use this for initialization
    void Start ()
    {
        NameText = GameObject.Find("NameText").GetComponent<Text>();
        NameText.text = Name;
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void LoadChrome()
    {
        SceneManager.LoadScene("Chrome");
    }
   

    public void LoadName()
    {
        SceneManager.LoadScene("EnterName");
    }

    public void LoadArticle()
    {
        SceneManager.LoadScene("Article");
    }
    
    public void LoadGlitchDog()
    {
        SceneManager.LoadScene("GlitchDog");
    }

    public void LoadChromeTree()
    {
        SceneManager.LoadScene("ChromeTree");
    }
    
    public void LoadYoutubeBegin()
    {
        SceneManager.LoadScene("YoutubeBegin");
    }

    public void LoadYoutubeReplies()
    {
        SceneManager.LoadScene("YoutubeReplies");
    }

    public void LoadYoutubeGlitch()
    {
        SceneManager.LoadScene("YoutubeGlitch");
        YoutubeDone = true;
        print("Youtube is true");
    }

    public void LoadFacebookStart()
    {
        SceneManager.LoadScene("FacebookStart");
    }

    public void LoadFacebookGlitch()
    {
        SceneManager.LoadScene("FacebookGlitch");
        FacebookDone = true;
        print("Facebook is true");
    }

    public void LoadTumblrStart()
    {
        SceneManager.LoadScene("TumblrStart");
    }

    public void LoadPop1()
    {
        SceneManager.LoadScene("Pop1");
    }
    
    public void LoadPop2()
    {
        SceneManager.LoadScene("Pop2");
    }

    public void LoadPop3()
    {
        SceneManager.LoadScene("Pop3");
    }

    public void LoadPop4()
    {
        SceneManager.LoadScene("Pop4");
    }

    public void LoadPop5()
    {
        SceneManager.LoadScene("Pop5");
    }
    
    public void LoadPop6()
    {
        SceneManager.LoadScene("Pop6");
    }

    public void LoadPop7()
    {
        SceneManager.LoadScene("Pop7");
    }

    public void LoadCrash()
    {
        SceneManager.LoadScene("CodeCrash");
        TumblrDone = true;
        print("tumblr is true");
    }

    public void LoadFinalPop()
    {
    if (FacebookDone == true && YoutubeDone == true && TumblrDone == true)
        {
            print("All are true");
            SceneManager.LoadScene("FinalPopUp");
        }
           
    }

    public void LoadTreeNoTumblr()
    {
        SceneManager.LoadScene("TreeNoTumblr");
    }

    public void LoadTreeNoYoutube()
    {
        SceneManager.LoadScene("TreeNoYoutube");
    }

    public void LoadTreeNoFacebook()
    {
        SceneManager.LoadScene("TreeNoFacebook");
    }
    public void LoadFinal()
    {
        SceneManager.LoadScene("FinalDog");
    }

    public void LoadSorry()
    {
        SceneManager.LoadScene("Sorry");
    }
    }

