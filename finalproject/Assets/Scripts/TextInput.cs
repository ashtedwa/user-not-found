﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextInput : MonoBehaviour {

    public InputField inputField;
    private Manager Manager; 
    

	// Use this for initialization
	void Awake () {

        Manager = GetComponent<Manager>();
        inputField.onEndEdit.AddListener(AcceptStringInput);
		
	}

    void AcceptStringInput(string userType)
    {
        Manager.ChangeName(userType);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
